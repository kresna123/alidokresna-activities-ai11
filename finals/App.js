import { StatusBar } from "expo-status-bar";
import {ImageBackground, Image, StyleSheet, Text, View, SafeAreaView,
  Button, Dimensions, TouchableOpacity, ImageStore,} from "react-native";
import { useState, useEffect, useRef } from "react";


const dipperAndPailWidth = 100;

export default function App() {
  // get the size of the device minus the pail and dipper width
  const windowWidth = Dimensions.get("window").width - dipperAndPailWidth;
  const windowHeight = Dimensions.get("window").height - dipperAndPailWidth;

  //before and after score state variables
  const beforeScoreRef = useRef(0);
  const afterScoreRef = useRef(0);

  const [play, setPlay] = useState(false);
  // There are two screen that we need one for the "home" screen and "game" screen
  const [screen, setScreen] = useState("home");

  // position and speed of dipper
  const [dipper, setDipper] = useState({
    top: 0,
    left: Math.floor(Math.random() * windowWidth),
    speed: 50,
  });
 
  const pailRef = useRef({
    left: windowWidth / 2,
  });


  useEffect(() => {
    // this condition will validate if the score is added or not, if beforescore is not equal to afterScore then the game continues
    if (beforeScoreRef.current != afterScoreRef.current && screen == "game") {
      beforeScoreRef.current = afterScoreRef.current;
      playHandler();
    }
  });

  // generate random position of dipper
  const playHandler = () => {
    setScreen("game");
    setPlay(true);

  const random = Math.floor(Math.random() * windowWidth);
  const interval = setInterval(() => {
      if (dipper.top >= windowHeight) {
        clearInterval(interval);
        if (
          pailRef.current.left - dipper.left > -25 &&
          pailRef.current.left - dipper.left < 25
        ) {
          afterScoreRef.current = beforeScoreRef.current + 1;

     // sets the default position of dipper
          setDipper({
            ...dipper,
            top: 0,
            left: random,
          });
        } else {
          setPlay(false);
          setDipper({
            ...dipper,
            top: 0,
            left: random,
          });
          return false;
        }
      } else {

        setDipper({
          ...dipper,
          top: (dipper.top += 10),
        });
      }
    }, dipper.speed);
  };

  // this function will trigger when you click the "TouchableOpacity" and set the location of the pail
  const pailHandler = (e) => {
    // pointX is the location of your cursor
    let pointX = null;
    if (e.nativeEvent.locationX) {
      pointX = e.nativeEvent.locationX;
    } else {
      pointX = e.nativeEvent.clientX;
    }
    console.log(pointX);
    // pointX must subtract with half of dipperAndPailWidth
    pailRef.current = { left: pointX - dipperAndPailWidth / 2 };
  };
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar style="auto" />
      {/* background Image  */}
      <ImageBackground
        source={require(`./assets/images/beach.jpg`)}
        resizeMode="cover"
        style={styles.bgImage}
      >
        {}
        {}
        {}
        {screen == "game" ? (
          <>
            <>
              {/* dipper image  */}
              {}
              <View style={[styles.dipperPosition]}>
                <Image
                  source={require(`./assets/images/dipper.png`)}
                  style={[
                    styles.dipperImage,
                    { left: dipper.left },
                    { top: dipper.top },
                  ]}
                />
              </View>
              {/* pail image  */}
              {}
              <View style={styles.pailPosition}>
                <Image
                  source={require(`./assets/images/pail.png`)}
                  style={[styles.pailImage, pailRef.current]}
                />
              </View>

              <Text></Text>
              <Text></Text>
              {}
              <Text style={styles.textLg}>Score: {afterScoreRef.current}</Text>
            </>
            {}
            <TouchableOpacity
              activeOpacity={1}
              style={styles.bgImage}
              onPress={(evt) => pailHandler(evt)}
            ></TouchableOpacity>
            {}
            {}
            {!play && (
              <View style={styles.gameOverContainer}>
                <View style={styles.gameOverCard}>
                  <Text style={styles.textLg}>Game Over!</Text>
                  <Text style={styles.textMd}>Your Score</Text>
                  <Text style={styles.textLg}>{afterScoreRef.current}</Text>
                  {}
                  <Button
                    style={styles.btn}
                    onPress={() => {
                      afterScoreRef.current = 0;
                      beforeScoreRef.current = 0;
                      playHandler();
                    }}
                    title="Play Again"
                  />
                  {}
                  <Button
                    style={styles.btn}
                    onPress={() => {
                      afterScoreRef.current = 0;
                      beforeScoreRef.current = 0;
                      setScreen("home");
                    }}
                    title="Back to Home"
                  />
                </View>
              </View>
            )}
          </>
        ) : (
          // user interface of home screen
          <View
            style={[
              styles.bgImage,
              { flex: 1, alignItems: "center", justifyContent: "center" },
            ]}
          >
            <View style={styles.gameOverCard}>
              <Text style={[styles.textLg]}>Pail Catch Me!</Text>
              <View style={{ textAlign: "center", alignItems: "center" }}>
                <Image
                  source={require(`./assets/images/dipper.png`)}
                  style={styles.pailImage}
                />
                <Image
                  source={require(`./assets/images/pail.png`)}
                  style={styles.pailImage}
                />
              </View>
              {}
              <Button
                style={styles.btn}
                onPress={playHandler}
                title="Start the game"
              />
            </View>
          </View>
        )}
      </ImageBackground>
    </SafeAreaView>
  );
}
// styling
const styles = StyleSheet.create({
  btn: {
    fontSize: 30,
    fontWeight: 600,
  },
  bigText: {
    fontSize: 20,
  },
  bgImage: {
    height: "100%",
    width: "100%",
  },
  dipperImage: {
    position: "absolute",
    width: dipperAndPailWidth,
    height: dipperAndPailWidth,
  },
  pailImage: {
    width: dipperAndPailWidth,
    height: dipperAndPailWidth,
  },
  dipperPosition: {
    position: "relative",
    bottom: 0,
  },
  pailPosition: {
    position: "absolute",
    bottom: 0,
  },
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  gameOverContainer: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 10,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(1, 1, 1, 0.5)",
  },
  textLg: {
    marginBottom: 10,
    fontWeight: "bold",
    textAlign: "center",
    fontSize: 30,
  },
  textMd: {
    textAlign: "center",
    marginBottom: 10,
    fontWeight: "600",
    fontSize: 20,
  },
  gameOverCard: {
    borderRadius: 10,
    backgroundColor: "white",
    padding: 20,
  },
});
