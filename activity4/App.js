import React from 'react';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

// Screens
import appIntro from './src/screens/appIntro'; 

const Stack = createNativeStackNavigator();

export default function App () {

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='app Intro' screenOptions={{headerShown: false}}>
          <Stack.Screen name="app Intro" component={appIntro} />
        </Stack.Navigator>
    </NavigationContainer>
  )
};