import React from 'react';
import { StyleSheet, Text, Image, View} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
 
const slides = [
  {
    key: '1',
    title: 'Entry 1',
    subtitle: 'Any profession requires knowledge of computers and software. Information technology (IT) is used by\n organizations for many reasons.',
    image: require('../images/pic1.png'),
  },
  {
    key: '2',
    title: 'Entry 2',
    subtitle: 'To disseminate information more quickly and effectively.',
    image: require('../images/pic2.png'),
  },
  {
    key: '3',
    title: 'Entry 3',
    subtitle: 'Help manage the dynamics of daily life.',
    image: require('../images/pic3.png'),
  },
  {
    key: '4',
    title: 'Entry 4',
    subtitle: 'Provides new, better and faster ways for people to interact, network, get help,  access  information, and learn.',
    image: require('../images/pic4.png'),
  },
  {
    
    key: '5',
    title: 'Entry 5',
    subtitle: 'Enable knowledge sharing globally.',
    image: require('../images/pic5.png'),
  },
];

const Slide = ({ item }) => {
    return (
      <View style={styles.slide}>
        <Text style={styles.title}>{item.title}</Text>
        <Image 
            source={item.image}
            style={{height: '75%', width: '100%', resizeMode: 'contain'}}
        />
        <Text style={styles.subtitle}>{item.subtitle}</Text>
      </View>
    );
};

export default function appIntro ({navigation}) {
    return (
        <AppIntroSlider
          data={slides}
          renderItem={({item}) => <Slide item={item}/>}
          onDone={()=> navigation.push('app Intro')} 

          activeDotStyle={{
            backgroundColor:"#21465b",
            width:10
          }}
          showDoneButton={true}
          renderDoneButton={()=><Text style={{color: 'red', margin: 20, fontSize: 18, fontWeight: 'bold'}}>Home</Text>}
          showNextButton={true}
          renderNextButton={()=><Text style={{color: 'red', margin: 20, fontSize: 18, fontWeight: 'bold'}}>Continue</Text>}
        />
    );
}

const styles = StyleSheet.create({
    slide: {
        alignItems: 'center',
        backgroundColor: 'transparent',
        marginTop: 50,
    },
    subtitle: {
        color: 'red',
        fontSize: 18,
        marginTop: 10,
        maxWidth: '75%',
        textAlign: 'center',
        lineHeight: 23,
    },
    title: {
        color: 'red',
        fontSize: 22,
        fontWeight: 'bold',
        marginTop: 10,
        textAlign: 'center',
    },
})