import * as React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';

export default function actualApp({navigation}) {
    return (
        <View style={styles.container}>
            <Text style={styles.screenTitle}>Home</Text>
            <Text style={styles.description}>Finish</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
      paddingTop: 50,
      paddingHorizontal: 20,
    },
    screenTitle: {
      fontSize: 26,
      fontWeight: 'bold',
      color: 'black',
    },
    description: {
      color: 'black',
      marginTop: 20,
      fontSize:30,
    },
});